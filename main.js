const grid = new Muuri(".grid", {
  layout: {
    rounding: true,
  },
});


window.addEventListener("load", () => {
  grid.refreshItems().layout;
  document.getElementById("grid").classList.add("images-loaded" );
  
  
  //Filtado por categoria
  
  const enlaces = document.querySelectorAll("#catalogo a");
  enlaces.forEach((element) => {
    element.addEventListener("click", (event) => {
        event.preventDefault();
        enlaces.forEach((enlace) => enlace.classList.remove("inicio"));
        event.target.classList.add("inicio")

        const content = event.target.innerHTML.toLowerCase();
        content === ("todos") ? grid.filter("[data-content]") : grid.filter(`[data-content="${content}"]`)
        

    });
  });
});


//Filtrado Barra de busqueda


document.querySelector("#barra-busqueda").addEventListener("input", (event) => {
  const busqueda  = event.target.value
  grid.filter((item) => ( item.getElement().dataset.label).includes(busqueda) )
})



//IMGENES OVERLAY

const overlay = document.getElementById("overlay")
document.querySelectorAll(".grid .item img").forEach((element) => {
  element.addEventListener("click", () => {
    const route = element.getAttribute("src")
    const description = element.parentNode.parentNode.dataset.description;
    
    overlay.classList.add("activo")
    document.querySelector("#overlay img").src = route;
    document.querySelector("#overlay .description").innerHTML= description;
  })

 
})

//BOTON DE CERRAR 

document.querySelector("#btn-close-popup").addEventListener("click", () => {
  overlay.classList.remove("activo")
});

overlay.addEventListener("click", (event) => {
 event.target.id === "overlay" ? overlay.classList.remove("activo"): "";
});